package restro_admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;

@Controller
public class HtmlController {

    @RequestMapping("/{pagename}.html")
    public String mainDirectoryPage(@PathParam(value="pagename") String pageName, Model model) {
        model.addAttribute("name", pageName);
        return pageName;
    }

    @RequestMapping("/{directory}/{pagename}.html")
    public String firstDirectoryPage(@PathParam(value="directory") String directory, @PathParam(value="pagename") String pageName, Model model) {
        model.addAttribute("name", pageName);
        return directory + "/" + pageName;
    }

    @RequestMapping("/{directory1}/{directory2}/{pagename}.html")
    public String firstDirectoryPage(@PathParam(value="directory1") String directory1,@PathParam(value="directory2") String directory2, @PathParam(value="pagename") String pageName, Model model) {
        model.addAttribute("name", pageName);
        return directory1 + "/" + directory2 + "/" +pageName;
    }
}

